//
//  Request.swift
//  Meteorite
//
//  Created by Ievgen on 30.11.16.
//  Copyright © 2016 Ievgen. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class Requests
{
    // Singleton constant
    static let sharedInstance = Requests()
    
    // Base api url
    var API_URL: String {
        return "https://data.nasa.gov/resource/y77d-th95.json?$limit=50000&$where=year%20%3E%20'2010-01-01T00:00:00.000'&$$app_token="
    }
    
    // Token
    private let token = "LLXwDcxuys1TsbEwa6SAgYdnv"
    
    func getDataFromNasa(completion: @escaping (_ response: [Meteorite]?) -> ())
    {
        let apiUrl = API_URL + token
        
        Alamofire.request(apiUrl).validate().responseJSON { [weak weakSelf = self] response in
            
            switch response.result {
                
            // Response is success
            case .success(let value):
                
                let json = JSON(value)
                let meteoritesArray = weakSelf?.parse(json)
                
                completion(meteoritesArray)
                
            // Error
            case .failure(let error):
                
                _ = Lgg(error)
                
                // Pass nil if response.result is not success
                completion(nil)
            }
        }
    }
    
    
    
    // Parse json
    private func parse(_ json: JSON) -> [Meteorite]
    {
        var meteoritesArray = [Meteorite]()
        
        let jsonArray = json
        for (_, item): (String, JSON) in jsonArray
        {
            var meteorite = Meteorite()
            
            if let name = item["name"].string {
                meteorite.name = name
            }
            
            if let id = item["id"].string {
                meteorite.id = id
            }
            
            if let recclass = item["recclass"].string {
                meteorite.recclass = recclass
            }
            
            if let weight = item["mass"].string,
                let doubleWeight = Double(weight) {
                meteorite.weight = doubleWeight
            }
            
            if let date = item["year"].string
            {
                let index = date.index(date.startIndex, offsetBy: 4)
                let year = date.substring(to: index)

                meteorite.year = year
            }
            
            if let long = item["reclong"].string,
                let doubleLong = Double(long) {
                meteorite.geoLocation.long = doubleLong
            }
            
            if let lat = item["reclat"].string,
                let doubleLat = Double(lat) {
                meteorite.geoLocation.lat = doubleLat
            }
            
            meteoritesArray.append(meteorite)
        }
        
        // Sort array
        meteoritesArray = meteoritesArray.sorted() { $0.weight > $1.weight }
        
        return meteoritesArray
    }
}
