//
//  Header.swift
//  Meteorite
//
//  Created by Ievgen on 12/2/16.
//  Copyright © 2016 Ievgen. All rights reserved.
//

import Foundation
import UIKit

protocol BackButtonDelegate: class
{
    func dismissViewController()
}

class Header: UIView
{
    // Title
    @IBOutlet weak var headerTitle: UILabel!
    
    // Activity indicator
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView?
    
    // Delegate
    weak var delegate: BackButtonDelegate?
    
    
    
    // MARK: - Animations
    func startActivityIndicatorAnimation()
    {
        guard let indicator = activityIndicator else { return }
        
        indicator.startAnimating()
        
        UIView.transition(with: indicator, duration: 0.3, options: .transitionCrossDissolve, animations: {
            
            indicator.isHidden = false
            
        }, completion: nil)
    }
    
    
    
    func stopActivityIndicatorAnimation()
    {
        guard let indicator = activityIndicator else { return }
        
        UIView.transition(with: indicator, duration: 0.3, options: .transitionCrossDissolve, animations: {
            
            indicator.isHidden = true
            
        }) { (true) in
            indicator.stopAnimating()
        }
    }
    
    
    
    // Back button action
    @IBAction func backButtonAction(sender: UIButton)
    {
        delegate?.dismissViewController()
    }
}
