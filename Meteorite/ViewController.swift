//
//  ViewController.swift
//  Meteorite
//
//  Created by Ievgen on 11/29/16.
//  Copyright © 2016 Ievgen. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    // Meteorites table view
    @IBOutlet weak var meteoritesTableView: UITableView!
    
    // Number of meteorite since 2011
    @IBOutlet weak var meteoritesCountLabel: UILabel!
    
    // Header view
    @IBOutlet weak var headerView: Header!
    
    // Refresh control for table view
    private var refreshControl = UIRefreshControl()
    
    // Data source for meteoritesTableView
    private var meteorites = [Meteorite]() {
        didSet {
            
            meteoritesCountLabel.text = "Meteorites: \(meteorites.count)"
            meteoritesTableView.reloadData()
        }
    }
    
    // Timestamp in milliseconds
    private var timeStamp: Int {
        return Int(Date().timeIntervalSince1970 * 1000)
    }
    
    
    
    // MARK: - viewDidLoad
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Add refreshControl to a table view
        refreshControl.addTarget(self, action: #selector(ViewController.requestNewData), for: .valueChanged)
        meteoritesTableView.addSubview(refreshControl)
        
        // Hide activity indicator
        headerView.activityIndicator?.isHidden = true
        
        // Notificate on applicationDidBecomeActive
        NotificationCenter.default.addObserver(self, selector: #selector(appBecomeActive), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
     
        // Request data
        requestNewData()
    }
    
    
    // Status bar style
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    
    
    // MARK: - Table view delegate and data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return meteorites.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        guard let cell = meteoritesTableView.dequeueReusableCell(withIdentifier: "meteoCell") as? MeteoriteTableViewCell else { return UITableViewCell() }

        // Hide seporator for the first cell in table view
        cell.meteorite = meteorites[indexPath.row]
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    
    
    // MARK: - Request new data
    
    func requestNewData()
    {
        //Start activity indicator
        headerView.startActivityIndicatorAnimation()
        
        Requests.sharedInstance.getDataFromNasa { [weak weakSelf = self] (response) in
            
            if let meteoritesArray = response
            {
                // Set data model for table view
                weakSelf?.meteorites = meteoritesArray
                
                // Remember timestamp if there was internet connection while loading data
                if Reachability.isConnectedToNetwork() {
                    UserDefaults.standard.set(weakSelf?.timeStamp, forKey: "Last data recieved timestamp")
                }
            }
                
            // If it fails loading data and response is nil
            else {
                
                // Check for internet connection and show alert
                let isConnected = Reachability.isConnectedToNetwork()
                isConnected ? weakSelf?.showAlertController(with: "Some problem occurred while loading data, please try again later") :
                                weakSelf?.showAlertController(with: "No Internet connection. Pull to refresh later")
            }
            
            // Stop activity indicators
            weakSelf?.refreshControl.endRefreshing()
            weakSelf?.headerView.stopActivityIndicatorAnimation() 
        }
    }
    
    
    
    //MARK: - Application did become active
    
    func appBecomeActive()
    {
        let rememberedTimeStamp = UserDefaults.standard.integer(forKey: "Last data recieved timestamp")
        let oneDayInMilliSeconds = 24 * 60 * 60 * 1000
        
        // Check if last request was sent more than 24h ago
        guard (rememberedTimeStamp != 0) && (timeStamp - rememberedTimeStamp) >= oneDayInMilliSeconds else { return }
        
        // Request new data
        requestNewData()
    }
    
    
    
    //MARK: - Show map view controller
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        guard segue.identifier == "Show map",
            let destinationVC = segue.destination as? MapViewController,
            let cell = sender as? MeteoriteTableViewCell else { return }
        
        destinationVC.meteoriteToShow = cell.meteorite
    }
    
    
    
    // MARK: - Alert controller
    
    private func showAlertController(with message: String)
    {
        // Title
        let alert = UIAlertController(title: message, message: nil, preferredStyle: .alert)
        
        // Ok action to dismiss alert controller
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { alertAction in
            
            alert.dismiss(animated: true, completion: nil)
            
        }))
        
        present(alert, animated: true, completion: nil)
    }
}
