//
//  MapViewController.swift
//  Meteorite
//
//  Created by Ievgen on 12/2/16.
//  Copyright © 2016 Ievgen. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController, BackButtonDelegate
{
    // Header
    @IBOutlet weak var headerView: Header!
    
    // Labels outlets
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var classLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    
    // Map view
    @IBOutlet weak var mapView: MKMapView!
    
    // Meteorite to show
    var meteoriteToShow = Meteorite() {
        didSet {
            // In some cases coordinates are (0.0,0.0), so let pretend these meteorites were found in Brno
            guard meteoriteToShow.geoLocation == (0.0,0.0) else { return }
            meteoriteToShow.geoLocation = (49.194896, 16.608195)
        }
    }
    
    
    // MARK: - ViewDidLoad
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Header delegate
        headerView.delegate = self

        // Update labeles
        updateUI(for: meteoriteToShow)
        
        // Placmark
        centerMap(on: meteoriteToShow)
        
        // Add anotation
        addAnotation(for: meteoriteToShow)
    }
    
    
    
    // Status bar style
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    
    
    // MARK: - Update UI
    
    private func updateUI(for meteorite: Meteorite)
    {
        // Header label
        headerView.headerTitle.text = meteorite.name
        
        // Main labels
        weightLabel.text = "Weight: \(meteorite.weight)g"
        yearLabel.text = "Year: " + meteorite.year
        classLabel.text = "Class: " + meteorite.recclass
        idLabel.text = "id: " + meteorite.id
    }
    
    
    
    // MARK: - Header view delegate
    
    func dismissViewController()
    {
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    
    
    // MARK: - Map parameters and anotation
    
    private func centerMap(on meteorite: Meteorite)
    {
        let location = CLLocation(latitude: meteorite.geoLocation.lat, longitude: meteorite.geoLocation.long)
        let regionRadius: CLLocationDistance = 100000
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    
    
    private func addAnotation(for meteorite: Meteorite)
    {
        let location = CLLocationCoordinate2DMake(meteorite.geoLocation.lat, meteorite.geoLocation.long)
        let pin = MKPointAnnotation()
        pin.coordinate = location
        pin.title = meteorite.name
        pin.subtitle = "Weight: \(meteorite.weight)g"
        mapView.addAnnotation(pin)
        mapView.selectAnnotation(pin, animated: true)
    }
}
