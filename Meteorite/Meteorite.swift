//
//  Meteorite.swift
//  Meteorite
//
//  Created by Ievgen on 11/29/16.
//  Copyright © 2016 Ievgen. All rights reserved.
//

import Foundation

struct Meteorite
{
    var name = ""
    var id = ""
    var recclass = ""
    var weight = 0.0
    var year = ""
    var geoLocation: (lat: Double, long: Double) = (0,0)
}
