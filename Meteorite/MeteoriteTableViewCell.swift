//
//  MeteoriteTableViewCell.swift
//  Meteorite
//
//  Created by Ievgen on 11/29/16.
//  Copyright © 2016 Ievgen. All rights reserved.
//

import UIKit
import MapKit

class MeteoriteTableViewCell: UITableViewCell
{
    // Labels outlets
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    
    // Seporator
    
    var meteorite = Meteorite() {
        didSet {
            nameLabel.text = meteorite.name
            weightLabel.text = "weight: \(meteorite.weight)g"
            yearLabel.text = "year: " + meteorite.year
        }
    }
}
