//
//  MeteoriteTests.swift
//  MeteoriteTests
//
//  Created by Ievgen on 11/29/16.
//  Copyright © 2016 Ievgen. All rights reserved.
//

import XCTest
import MapKit
import CoreLocation
@testable import Meteorite

class MeteoriteTests: XCTestCase {
    
    var mainVC: ViewController!
    var mapVC: MapViewController!
    var meteorite = Meteorite()
    var request: RequestsForTesting!
    
    // New subclass to test Requests
    class RequestsForTesting: Requests {
        
        override var API_URL: String { return "Some Broken URL" }
    }
    
    
    override func setUp()
    {
        super.setUp()
        
        mainVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainVC") as! ViewController
        mapVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MapVC") as! MapViewController
        request = RequestsForTesting()
        
        // Create test Meteorite object
        meteorite.name = "Test Name"
        meteorite.recclass = "Test class"
        meteorite.weight = 11235813.0
        meteorite.year = "Test year"
        meteorite.id = "Test id"
        meteorite.geoLocation.lat = 49.194896
        meteorite.geoLocation.long = 16.608195

    }
    
    
    override func tearDown()
    {
        super.tearDown()
        
        mainVC = nil
        mapVC = nil
        request = nil
    }
    
    
    func testViewControllerDidLoad()
    {
        XCTAssertNotNil(self.mainVC.view, "View did not load for ViewController")
    }
    
    
    func testMapVCDidLoad()
    {
        XCTAssertNotNil(self.mapVC.view, "View did not load for MapViewController")
    }
    
    
    // Testing Requests "getDataFromNasa" to return nil
    func testGetDataFromServerPassesNilIfError()
    {
        request.getDataFromNasa { (response) in
            XCTAssertNil(response)
        }
    }
    
    
    func testMapVCLabelValuesShowedProperly()
    {
        mapVC.meteoriteToShow = meteorite
        
        let _ = mapVC.view
        
        XCTAssert(mapVC.headerView.headerTitle.text == "Test Name" , "headerTitleLabel doesn't show the right text ")
        XCTAssert(mapVC.classLabel.text == "Class: Test class" , "classLabel doesn't show the right text ")
        XCTAssert(mapVC.weightLabel.text == "Weight: 11235813.0g" , "weightLabel doesn't show the right text ")
        XCTAssert(mapVC.yearLabel.text == "Year: Test year" , "yearLabel doesn't show the right text ")
        XCTAssert(mapVC.idLabel.text == "id: Test id" , "idLabel doesn't show the right text ")
    }
    
    
    func testMapViewPinIsSetProperly()
    {
        mapVC.meteoriteToShow = meteorite
        let _ = mapVC.view
        
        //Create Testpin
        let testLocation = CLLocationCoordinate2DMake(meteorite.geoLocation.lat, meteorite.geoLocation.long)
        let testPin = MKPointAnnotation()
        testPin.coordinate = testLocation
        testPin.title = meteorite.name
        testPin.subtitle = "Weight: \(meteorite.weight)g"
        
        let pin = mapVC.mapView.annotations.first!
        
        XCTAssert(testPin.title == pin.title!, "Pin title doesn't show the righ text")
        XCTAssert(testPin.subtitle == pin.subtitle!, "Pin subtitle doesn't show the righ text")
        XCTAssert(testPin.coordinate.latitude == pin.coordinate.latitude, "Latitude coordinate is wrong")
        XCTAssert(testPin.coordinate.longitude == pin.coordinate.longitude, "Longitude coordinate is wrong")
    }
    
    
    func testHeaderBackButtonDelegateIsSet()
    {
        let _ = mapVC.view
        XCTAssertNotNil(mapVC.headerView.delegate, "hedaerView delegate is nil")
    }
    
    
    func testActivityIndicatorIsAnimating()
    {
        let _ = mainVC.view
        
        XCTAssert(mainVC.headerView.activityIndicator!.isAnimating, "Activity indicator is not animating")
    }
}
